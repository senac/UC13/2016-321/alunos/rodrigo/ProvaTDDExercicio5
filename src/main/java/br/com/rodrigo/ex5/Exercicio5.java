/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.ex5;

import java.util.Scanner;


/**
 *
 * @author Diamond
 * Faça um programa que leia um número inteiro e mostre uma mensagem indicando se
este número é par ou ímpar, e se é positivo ou negativo.
 */
public class Exercicio5 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numero;

        System.out.print("Digite um número inteiro: ");
        numero = entrada.nextInt();

        if (numero % 2 == 0) {
            System.out.print("O número é par.\n");
        } else {
            System.out.print("O número é Impar.\n");
        }
        if (numero > 0) {
            System.out.print("O número é positivo.\n");
        } else {
            System.out.print("O número é negativo.\n");
        }

    }
}
